<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Entering details</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">
    <link href="css/item1.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<%@ page import="cw.*" %>
<%!
film fi;
search_manager s;
%>
<%
try
{
String name="";
String action="";
String thriller="";
String horror="";
String comedy="";
String romcom="";
String animation="";
String drama="";
String documentary="";
String scifi="";
String m="";
fi=new film();
name = request.getParameter("name");
action = request.getParameter("action");
thriller = request.getParameter("thriller");
horror = request.getParameter("horror");
comedy = request.getParameter("comedy");
romcom = request.getParameter("romcom");
animation = request.getParameter("animation");
drama = request.getParameter("drama");
documentary = request.getParameter("documentary");
scifi = request.getParameter("scifi");
String year = request.getParameter("year");
String lang = request.getParameter("language");
String syn = request.getParameter("synopsis");
String trailer = request.getParameter("trailer");
String maker = request.getParameter("maker");
int y=Integer.parseInt(year);
String synop = request.getParameter("synopsis");
String cast = request.getParameter("cc");
String tra = request.getParameter("trailer");
out.println("Details of the film");
out.println(name+" "+maker+" "+y+" "+action+" "+thriller+" "+horror+" "+comedy+" "+romcom+" "+animation+" "+drama+" "+documentary+" "+scifi+" "+lang);
fi.setName(name);
fi.setYear(y);
if(!(action==null))
    fi.setAction(1);
else
    fi.setAction(0);
if(!(thriller==null))
    fi.setThriller(1);
else
    fi.setThriller(0);
if(!(horror==null))
    fi.setHorror(1);
else
    fi.setHorror(0);
if(!(comedy==null))
    fi.setComedy(1);
else
    fi.setComedy(0);
if(!(romcom==null))
    fi.setRomcom(1);
else
    fi.setRomcom(0);
if(!(animation==null))
    fi.setAnimation(1);
else
    fi.setAnimation(0);
if(!(drama==null))
    fi.setDrama(1);
else
    fi.setDrama(0);
if(!(documentary==null))
    fi.setDocumentary(1);
else
    fi.setDocumentary(0);
if(!(scifi==null))
    fi.setScifi(1);
else
    fi.setScifi(0);
fi.setLanguage(lang);
fi.setMaker(maker);
fi.setSynopsis(syn);
fi.setCast(cast);
fi.setTrailer(tra);
out.println(cast+" "+synop+" "+fi.getCast()+" "+fi.getSynopsis()+" "+fi.getPoster());
s = new search_manager();
s.insert(fi);
}
catch(Exception e)
{
    out.println(e);
}
%>
<form name="poster" action="posterUpload.html">
    Please upload the poster with the following file name: <%=fi.getPoster()%>
    <input type="submit">Proceed</input>
</form>
</body>
</html>