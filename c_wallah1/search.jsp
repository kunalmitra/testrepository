<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Result</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<%@ page import="java.sql.*" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="cw.*" %>
<%!
String driver = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost/C_Wallah";
String name1 = "root";
String pass = "root";
film results[] = new film[100];
search_manager s;
%>
<%
try
{
String query="1";
//out.println("before<br>");
String name="";
String action="";
String thriller="";
String horror="";
String comedy="";
String romcom="";
String animation="";
String drama="";
String documentary="";
String scifi="";
String m="";
name = request.getParameter("name");
action = request.getParameter("action");
thriller = request.getParameter("thriller");
horror = request.getParameter("horror");
comedy = request.getParameter("comedy");
romcom = request.getParameter("romcom");
animation = request.getParameter("animation");
drama = request.getParameter("drama");
documentary = request.getParameter("documentary");
scifi = request.getParameter("scifi");
String year = request.getParameter("year");
String lang = request.getParameter("language");
//out.println("after<br>"+name+"<br>"+action+"<br>"+thriller+"<br>"+comedy+"<br>"+romcom+"<br>");
Class.forName( driver );
Connection con = DriverManager.getConnection( url, name1, pass );
Statement stat = con.createStatement();
//out.println(query);
query="";
query = "SELECT * FROM film WHERE approved=1 and";
if(!(name==""))
query = query+" NAME='"+name+"' AND";
if(!(action==null))
query = query+" ACTION=1 AND";
if(!(thriller==null))
query = query+" THRILLER=1 AND";
if(!(horror==null))
query = query+" HORROR=1 AND";
if(!(comedy==null))
query = query+" COMEDY=1 AND";
if(!(romcom==null))
query = query+" ROMCOM=1 AND";
if(!(animation==null))
query = query+" ANIMATION=1 AND";
if(!(drama==null))
query = query+" DRAMA=1 AND";
if(!(documentary==null))
query = query+" DOCUMENTARY=1 AND";
if(!(scifi==null))
query = query+" SCIFI=1 AND";
//out.println(query);
if((lang!=null))
{
query+=" lang='"+lang+"'";
}
else
{
int last =  query.lastIndexOf(" ");
query = query.substring(0,last);
}
query = query+";";
//out.println(query);
s = new search_manager();
results = s.search(query);
//out.println("size "+s.size);
}
catch(Exception e)
{
//out.println("Error: "+e);
}
%>

    <!-- Navigation -->
    
    <!-- Page Content -->
    <div class="container search">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12 search-heading">Search results - 
                
                    <div class="col-md-4">
                        <a href="index.html"><img src="img/logo.jpg" width="50%"</a>
                        </div>
                   
                
            </div>
        </div>
       

        <!-- Related Projects Row -->
        <div class="row search-row">


            <%
            try{
            for(int i=0;i<s.size;i++)
            {
            out.println("<div class='col-sm-3 col-xs-6'>");
            out.println("<a href=film_page.jsp?name="+results[i].getName()+"&year="+results[i].getYear()+">");
            out.println("<img class='img-responsive portfolio-item' src = 'img/"+results[i].getPoster()+"'");
            out.println("<br>"+results[i].getNameClean()+"("+results[i].getYear()+")");
            out.println("</a>");
            out.println("</div>");
            }
            }
            catch(Exception e)
            {
            out.println("<br>Error: "+e);
             }

             %>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
