<!DOCTYPE html>
<%@ page import="java.sql.*" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="cw.*" %>
<%!
String driver = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost/C_Wallah";
String name1 = "root";
String pass = "root";
film fi[]=new film[1];
review r[]=new review[10];
search_manager s;
%>
<%
Class.forName( driver );
Connection con = DriverManager.getConnection( url, name1, pass );
Statement stat = con.createStatement();
String query = "select * from film where name = '"+request.getParameter("name")+"' and year = "+request.getParameter("year")+";";
try{
s = new search_manager();
fi= s.search(query);
r=s.getReview(fi[0]);
}
catch(Exception e){out.println(query);}
%> 



<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Film Page</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

   
    <!-- Page Content -->
    <div class="container">

       

            <div class="col-md-8">
                
                    
                <iframe class="film-frame" width="96%" height="350px" src="<%=fi[0].getTrailer()%>" frameborder="0" allowfullscreen></iframe>
                   
                    <div class="col-md-4 col-xs-4 film-banner">
                        <a href="index.html">"<img class="img-responsive" src="img/logo.jpg" width="100%;" style="margin-bottom:120px;" ></a>
                        
                    </div>
                <div class="col-md-12"> 
                    <h2><b><%=fi[0].getNameClean()%>(<%=fi[0].getYear()%>)</b>
                            <button class="btn btn-success btn-lg">
                                <%=fi[0].getCWScore()%>
                            </button>
                        
                        </h2>
                </div>
                
                
                
                    
                <div class="col-md-12 col-xs-12 film-search">
                    <div class="col-md-4 col-xs-6" >
                        <img src="img/<%=fi[0].getPoster()%>" alt="image1" width="100%" height="260px">
                        <i>Some Text Some Text Some Text Some Text Some Text Some Text Some Text Some Text Some Text Some Text  </i>
                    </div>
                        <div class="col-md-8 col-xs-6">
                        <img class="img-responsive" src="img/critics.png" width="100%;" >
                        <%
                        for(int i=0;i<s.size;i++)
                        {
                            out.println("<div class='col-md-5 film-review'>");
                            out.println(r[i].getSite());
                            if(r[i].getScore()==0)
                                out.println("(nay)");
                            else
                                out.println("(yay)");
                            out.println("<br>");
                            out.println(r[i].getDesc());
                            out.println("<a href='"+r[i].getLink()+"'>"+r[i].getLink()+"></a>");
                            out.println("</div>");
                            out.println("</div>");
                          }
                           %>
                </div>
                
            </div>

            <div class="col-md-4 col-xs-12" style="">
                <div class="col-md-12 film-cc">
                    <h4 >Cast</h4>
                    <p><%=fi[0].getCast()%></p>
                    
                    
                </div>
                <div class="col-md-12 film-syn">
                    <h4 >Synopsis</h4>
			<p><%=fi[0].getSynopsis()%><p>
                        </div>
                <div class="col-md-12 film-dir">
                    <h4 >Meet the Director</h4>
		    <p><a href ="actual_filmmaker_page.jsp?maker=<%=fi[0].getMaker().replaceAll(" ","_")%>"><%=fi[0].getMaker()%></p>
                    </div>
            </div>

        </div>

        

    
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
