<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome Filmmaker</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">
    <link href="css/item1.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="col-md-4">
                        <a href="index.html"><img src="img/logo.jpg" width="50%"></a>
                        </div>
                    
<%@page import="cw.*"%>
<%!
review r;
search_manager s;
%>
<%
{
r=new review();
r.setName(request.getParameter("name"));
r.setLink(request.getParameter("link"));
r.setDesc(request.getParameter("desc"));
r.setSite(request.getParameter("site"));
r.setYear(Integer.parseInt(request.getParameter("year")));
r.setScore(Integer.parseInt(request.getParameter("score")));
s = new search_manager();
s.insertReview(r);
out.println("Success");
}

%>
</body>
</html>