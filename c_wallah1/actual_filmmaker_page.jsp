<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome Filmmaker</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">
    <link href="css/item1.css" rel="stylesheet">
    <link href="css/item2.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<%@ page import="cw.*" %>
<%!
film_maker fm;
search_manager s;
%>
<%
fm = new film_maker();
s = new search_manager();
String temp = request.getParameter("maker");
fm = s.getMaker(temp.replaceAll("_"," "));
%>
    <!-- Navigation -->
    
    <!-- Page Content -->
    <div class="container search after_login">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12 search-heading">
                
                    <div class="col-md-3">
                        <a href="index.html"><img src="img/logo.jpg" width="69%"></a>
                        </div>
                <div class="col-md-6 heading1">
                        <h1><%=fm.getNameClean()%> </h1>
                        <div class="col-md-12">
                        <a href="index.html"><img src="img/<%=fm.getName()%>.jpg" width="69%"></a>
                        </div>
                </div>
                    
        </div>
       

        <!-- Related Projects Row -->
        
            <div class="col-lg-7 login4 left">
                <h1>Bio</h1>
                <br>
                <br>
                <p>
                <%=fm.getBio()%>    
                </p>
            </div>
            <div class="col-md-5 login4 right">
                <h1>Filmography</h1>
                <br><br>
                <%
                try
                {
                for(int i=0;i<fm.list_of_films.length;i++)
                    {
                    out.println("<div class='col-sm-3 col-xs-6'>");
                    out.println("<a href=film_page.jsp?name="+fm.list_of_films[i].getName()+"&year="+fm.list_of_films[i].getYear()+">");
                    out.println("<img class='img-responsive portfolio-item' src = 'img/"+fm.list_of_films[i].getPoster()+"'");
                    out.println("<br>"+fm.list_of_films[i].getNameClean()+"("+fm.list_of_films[i].getYear()+")");
                    out.println("</a>");
                    out.println("</div>");
                    }
                 }
                catch(Exception e)
                {
                //out.println("<br>Error: "+e);
                }
                %>    
            </div>
            
        <!-- /.row -->

        
        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
  

</body>

</html>
