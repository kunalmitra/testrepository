<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Film page</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">
    <link href="css/item1.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    
    <!-- Page Content -->
    <div class="container search after_login">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12 search-heading">
                
                    <div class="col-md-3">
                        <a href="index.html"><img src="img/logo.jpg" width="69"></a>
                        </div>
                    <div class="col-md-6">
                    <h1>Create Film Profile</h1>
                </div>
                
                    
        </div>
		</div>
       

        <!-- Related Projects Row -->
        
        <div class="col-lg-12 login3">
            
            <form name = "create film" action="makeFilm.jsp" method="get">
                <input type="hidden" name="maker" value="<%=request.getParameter("maker")%>">
                <div class="col-md-8">
                <div class="form-group">
                    <label for="inputPassword" class="control-label">Name of the film</label>
                    <div class="form-inline row">
                      <div class="form-group col-sm-6">
                        <input type="text" class="form-control" name = "name" id="inputPassword" placeholder="" >
                        
                      </div>
					  <label for="year" class="control-label">Year</label>
                      				<input list="years" name="year" id="year" style="width:100px">
										<datalist id="years">
											<option value="2017">
											<option value="2016">
											<option value="2015">
											<option value="2014">
											<option value="2013">
										</datalist>
                    </div>
                 </div>
			</div>
                <div class = "from-group col-md-4">
                  <div class="form-group">
                    <label class="control-label">Language</label>
                    	<input list="language" name="language" style="width:100px">
						<datalist id="language">
							<option value="English">
							<option value="Hindi">
							<option value="Bengali">
							<option value="Marathi">
							<option value="Punabi">
							<option value="Tamil">
							<option value="Malayalam">
							<option value="Gujarati">
							<option value="Assamese">
							<option value="Kannada">
						</datalist>
                        
						
             
                    </div>
				</div>
                <div class = "col-md-12">
					<label class = "control-label">Genre</label>
					<div class="checkbox">
                              
                              <label><input type="checkbox" name="action">Action</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <label><input type="checkbox" name="animation">Animation</label><br>
                              <label><input type="checkbox" name="comedy">Comedy</label>&nbsp;&nbsp;&nbsp;
                              <label><input type="checkbox" name="documentary">Documentary</label><br>
                              <label><input type="checkbox" name="drama">Drama</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <label><input type="checkbox" name="horror">Horror</label><br>
                              <label><input type="checkbox" name="romcom">RomCom</label>&nbsp;&nbsp;
                              <label><input type="checkbox" name="scifi">SciFi</label><br>
                              <label><input type="checkbox" name="thriller">Thriller</label>
                            </div>
							</div>
				
                <div class = "col-md-12">
                    <label class="control-label">Synopsis</label>
                    <textarea class="form-control big-ip" name="synopsis" placeholder="" ></textarea>
                 </div>
                <div class="col-md-12">
                    <label  class="control-label">Cast and Crew</label>
                    <textarea class="form-control big-ip" name ="cc" placeholder="" ></textarea> 
                </div>
                
                <div class="col-md-12">
                    <label for="inputPassword" class="control-label">Poster</label>
                    
                      <div class="col-md-12">
                        <input type="file"  name="fileToUpload" id="fileToUpload">
                        
                      </div>
                    </div>
                 
				 <hr>
				 <p>
				 </p>
                
                <div class="col-md-12">
                    <label class="control-label">Trailer Link</label>
                    <input type="text" class="form-control" name = "trailer" id="cc" placeholder="">
                  </div>
                  <hr>
				  <hr>
                
                <div class = "col-md-12">
					<input type = "submit">
					</div>
					
				</div>
               
        <!-- /.row -->

        
        
    
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    
    <script>
        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });
        
        
        
        
    </script>
    
  

</body>

</html>
