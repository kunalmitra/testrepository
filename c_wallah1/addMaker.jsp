<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Entering details</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">
    <link href="css/item1.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<%@ page import="cw.*" %>
<%!
film_maker fm;
search_manager s;
%>
<%
try
{
fm = new film_maker();
fm.setName(request.getParameter("maker"));
fm.setBio(request.getParameter("bio"));
fm.setId(0);
fm.setListOfFilms();
s = new search_manager();
s.insertMaker(fm);
}
catch(Exception e)
{
    out.println(e);
}
%>
<form name="poster" action="displayUpload.html">
    Please upload the poster with the following file name: <%=fm.getName()%>
    <input type="submit">Proceed</input>
</form>
</body>
</html>