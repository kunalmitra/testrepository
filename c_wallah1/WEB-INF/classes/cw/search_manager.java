package cw;




import java.sql.*;
import java.sql.DriverManager;
public class search_manager
{
public int size;
public String driver;
public String url;
public String name1;
public String pass;
public Connection con;
public Statement stat;
public PreparedStatement ps;
public search_manager()throws Exception
{
driver = "com.mysql.jdbc.Driver";
url = "jdbc:mysql://localhost:3306/C_Wallah";
name1 = "root";
pass = "root";
Class.forName(driver);
con = DriverManager.getConnection(url, name1, pass);
}
public film[] search(String s)throws Exception
{
film f[] = new film[100];
int c=0;
stat = con.createStatement();
ResultSet rs = stat.executeQuery(s);
while(rs.next())
{
f[c] = new film();
f[c].setName(rs.getString("name"));
f[c].setLanguage(rs.getString("lang"));
f[c].setYear(rs.getInt("year"));
f[c].setAction(rs.getInt("action"));
f[c].setHorror(rs.getInt("horror"));
f[c].setThriller(rs.getInt("thriller"));
f[c].setComedy(rs.getInt("comedy"));
f[c].setRomcom(rs.getInt("romcom"));
f[c].setAnimation(rs.getInt("animation"));
f[c].setDrama(rs.getInt("drama"));
f[c].setDocumentary(rs.getInt("documentary"));
f[c].setScifi(rs.getInt("scifi"));
f[c].setMaker(rs.getString("film_maker"));
f[c].setPoster(rs.getString("poster"));
f[c].setCast(rs.getString("cast"));
f[c].setSynopsis(rs.getString("synopsis"));
f[c].setTrailer(rs.getString("trailer"));
f[c].setApproved(rs.getInt("approved"));
f[c].setCWScore();
c++;
}
this.size=c;
return f;
}
public void insert(film fi)throws SQLException
{
fi.setPoster(fi.getName()+".jpg");
ps = con.prepareStatement("INSERT INTO film VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
ps.setString(1, fi.getName());
ps.setInt(12, fi.getYear());
ps.setInt(2, fi.getAction());
ps.setInt(7, fi.getHorror());
ps.setInt(10, fi.getThriller());
ps.setInt(4, fi.getComedy());
ps.setInt(8, fi.getRomcom());
ps.setInt(3, fi.getAnimation());
ps.setInt(6, fi.getDrama());
ps.setInt(5, fi.getDocumentary());
ps.setInt(9, fi.getScifi());
ps.setString(11,fi.getLanguage());
ps.setString(15, fi.getMaker());
ps.setString(13,fi.getName()+".jpg");
ps.setInt(14,0);
ps.setString(16,fi.getSynopsis());
ps.setString(17,fi.getCast());
ps.setString(18,fi.getTrailer());
ps.setInt(19,fi.getApproved());
ps.executeUpdate();
}
public void insertReview(review r)throws Exception
{
ps = con.prepareStatement("INSERT INTO review VALUE(?,?,?,?,?,?)");
ps.setString(1,r.getName());
ps.setString(2,r.getLink());
ps.setString(3,r.getDesc());
ps.setString(4,r.getSite());
ps.setInt(5,r.getYear());
ps.setInt(6,r.getScore());
ps.executeUpdate();
}
public review[] getReview(film f)throws Exception
{
review r[]=new review[100];
int c=0;
String s = "select * from review where name='"+f.getName()+"' and year="+f.getYear()+";";
stat = con.createStatement();
ResultSet rs = stat.executeQuery(s);
while(rs.next())
{
r[c]=new review();
r[c].setName(rs.getString("name"));
r[c].setLink(rs.getString("link"));
r[c].setDesc(rs.getString("descr"));
r[c].setSite(rs.getString("site_name"));
r[c].setYear(rs.getInt("year"));
r[c].setScore(rs.getInt("score"));
c++;
}
this.size=c;
return r;
}
public void approve(film f)throws Exception
{
ps = con.prepareStatement("update film set approved = 1 where name = ? and year= ?;");
ps.setString(1,f.getName());
ps.setInt(2,f.getYear());
ps.executeUpdate();
}
public void insertMaker(film_maker fm)throws Exception
{
ps = con.prepareStatement("insert into film_maker value (?,?,?)");
ps.setInt(1,fm.getId());
ps.setString(2,fm.getNameClean());
ps.setString(3, fm.getBio());
ps.executeUpdate();
}
public film_maker getMaker(String m)throws Exception
{
film_maker fm=new film_maker();
String s = "select * from film_maker where name='"+m+"';";
stat = con.createStatement();
ResultSet rs = stat.executeQuery(s);
rs.next();
fm.setName(rs.getString("name"));
fm.setId(rs.getInt("id"));
fm.setBio(rs.getString("bio"));
fm.setListOfFilms();
return fm;
}
}