package cw;

public class review
{
String name,link,desc,site_name;
int year, score;

public review()
{
name="";
link="";
desc="";
site_name="";
year=0;
score=0;
}

public void setName(String n)
{
this.name = n;
}
public void setLink(String l)
{
this.link = l;
}
public void setDesc(String d)
{
this.desc = d;
}
public void setSite(String s)
{
this.site_name = s;
}
public void setYear(int y)
{
this.year = y;
}
public void setScore(int s)
{
this.score = s;
}
public String getName()
{
    return this.name;
}
public String getLink()
{
    return this.link;
}
public String getDesc()
{
    return this.desc;
}
public String getSite()
{
    return this.site_name;
}
public int getYear()
{
    return this.year;
}
public int getScore()
{
    return this.score;
}
}