package cw;




public class film_maker
{
String name;
int id;
String bio;
public film[] list_of_films;
public film_maker()
{
name="";
id=0;
list_of_films=new film[100];
}
public void setName(String n)
{
this.name = n;
}
public void setId(int i)
{
this.id= i;
}
public void setBio(String b)
{
this.bio = b;
}
public void setListOfFilms()throws Exception
{
search_manager s = new search_manager();
list_of_films = s.search("SELECT * FROM film WHERE film_maker='"+this.name+"';");
}
public String getName()
{
String temp = this.name.replaceAll(" ", "_");
return temp;
}
public String getNameClean()
{
return this.name;
}
public int getId()
{
return this.id;
}
public String getBio()
{
return this.bio;
}
public film[] getListOfFilms()
{
return this.list_of_films;
}
}