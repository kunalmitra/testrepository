package cw;




public class film
{
String name;
int year,action,horror,thriller,comedy,romcom,drama,animation,documentary,scifi,approved;
String maker,language,poster,synopsis,cast,trailer;
double cwscore;
public film()
{
name="";
language="";
maker="";
poster="";
synopsis="";
cast="";
trailer="";
cwscore=0;
year=0;
action=0;
horror=0;
thriller=0;
comedy=0;
romcom=0;
drama=0;
animation=0;
documentary=0;
scifi=0;
approved = 0;
}
public void setName(String n)
{
n = n.replaceAll(" ", "_");
this.name=n;
}
public void setLanguage(String l)
{
this.language=l;
}
public void setPoster(String p)
{
this.poster = p;
}
public void setYear(int y)
{
this.year=y;
}
public void setAction(int a)
{
this.action = a;
}
public void setThriller(int t)
{
this.thriller = t;
}
public void setHorror(int h)
{
this.horror = h;
}
public void setComedy(int c)
{
this.comedy = c;
}
public void setRomcom(int r)
{
this.romcom = r;
}
public void setAnimation(int an)
{
this.animation = an;
}
public void setDrama(int d)
{
this.drama = d;
}
public void setDocumentary(int d)
{
this.documentary = d;
}
public void setScifi(int s)
{
this.scifi = s;
}
public void setCWScore() throws Exception
{
search_manager s = new search_manager();
review r[]=new review[100];
r=s.getReview(this);
double c=0;
int t=0;
for(int i=0;i<s.size;i++)
{
t=r[i].getScore();
c+=t;
}
if(s.size!=0)
{
    c=(c/s.size)*100;
    this.cwscore=c;
}
else
this.cwscore=0;
}
public void setMaker(String m)
{
this.maker = m;
}
public void setSynopsis(String s)
{
this.synopsis = s;
}
public void setCast(String c)
{
this.cast = c;
}
public void setTrailer(String t)
{
this.trailer = t;
}
public void setApproved(int a)
{
    this.approved=a;
}
public String getName()
{
return this.name;
}
public String getNameClean()
{
String n = this.name;
n = n.replaceAll("_", " ");
return n;
}
public String getLanguage()
{
return this.language;
}
public String getMaker()
{
return this.maker;
}
public String getPoster()
{
return this.poster;
}
public int getYear()
{
return this.year;
}
public double getCWScore()
{
return this.cwscore;
}
public int getAction()
{
return this.action;
}
public int getHorror()
{
return this.horror;
}
public int getThriller()
{
return this.thriller;
}
public int getComedy()
{
return this.comedy;
}
public int getRomcom()
{
return this.romcom;
}
public int getAnimation()
{
return this.animation;
}
public int getDocumentary()
{
return this.documentary;
}
public int getDrama()
{
return this.drama;
}
public int getScifi()
{
return this.scifi;
}
public String getSynopsis()
{
return this.synopsis;
}
public String getCast()
{
return this.cast;
}
public String getTrailer()
{
return this.trailer;
}
public int getApproved()
{
    return this.approved;
}
}