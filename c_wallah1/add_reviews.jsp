<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Approved films</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<%@ page import="cw.*" %>
<%!
film results[] = new film[100];
search_manager s;
%>
<%
try{
String q = "select * from film where approved = 1";
s=new search_manager();
results = s.search(q);
}
catch(Exception e)
{
    out.println(e);
}
%>        
<div class="container search">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12 search-heading">
                
                    <div class="col-md-4">
                        <a href="index.html"><img src="img/logo.jpg" width="50%"</a>
                        </div>
                  
                
            </div>
        </div>
       

        <!-- Related Projects Row -->
        <div class="row search-row">


            <%
            try{
            for(int i=0;i<s.size;i++)
            {
            out.println("<div class='col-sm-3 col-xs-6'>");
            out.println("<a href=film_page_admin.jsp?name="+results[i].getName()+"&year="+results[i].getYear()+">");
            out.println("<img class='img-responsive portfolio-item' src = 'img/"+results[i].getPoster()+"'");
            out.println("<br>"+results[i].getNameClean()+"("+results[i].getYear()+")");
            out.println("</a>");
            out.println("</div>");
            }
            }
            catch(Exception e)
            {
            out.println("<br>Error: "+e);
             }

             %>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    </body>
</html>
